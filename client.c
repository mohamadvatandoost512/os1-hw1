#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <netdb.h>
#include <fcntl.h>
#include <signal.h>

#define PORT 6576
#define MAX_BUFFER_SIZE 5000

#define GET_GROUPS_LIST 'G'
#define GET_USERS_LIST 'U'
#define JOIN_GROUP 'J'
#define CREATE_GROUP 'N'
#define JOIN_PRIVATE_CHAT 'P'
#define GROUP_EXIT 'E'
#define PRIVATE_CHAT_EXIT 'C'

#define PRIVATE_CHAT 'S'
#define GROUP_CHAT 'T'
#define GROUP_UN_ACTIVE 'F'
#define GROUP_REFRESH 'R'

int ttyfd, logfd;
uint8_t pvChat = 0;
int gChat = 0;
int gSocketfd = 0;;
struct sockaddr_in send_addr;
int serverSocketfd = 0;

int sendToServer(int clientfd, char buf_send[])
{
//        printf("sendToServer: %s, size: %d\n", buf_send, strlen(buf_send));
        if ( send(clientfd, (void *) (buf_send), strlen (buf_send), 0) < 0) {
            return -1;
        }
//        printf("sended successfully\n");
     return 0;
}

void sendGroupMessage(int clientfd, char buf_send[]) {

    if ( sendto(clientfd, buf_send, strlen(buf_send)+1, 0, (struct sockaddr*) &send_addr, sizeof send_addr)  < 0) {
         write(ttyfd, "sendGroupMessage can not send\n", strlen("sendGroupMessage can not send\n"));
    } else {
         write(ttyfd, "sendGroupMessage successfully\n", strlen("sendGroupMessage successfully\n"));
    }

}

int createGroupSocket() {
    struct sockaddr_in addr;
    int addrlen, sock, cnt;
    struct ip_mreq mreq;

    memset(&send_addr, 0, sizeof send_addr);
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = (in_port_t) htons(gChat);
    inet_aton("127.255.255.255", &send_addr.sin_addr);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
       if (sock < 0) {
         write(ttyfd, "group socket can not create\n", strlen("group socket can not create\n"));
         exit(1);
       }
       bzero((char *)&addr, sizeof(addr));
       addr.sin_family = AF_INET;
       addr.sin_addr.s_addr = htonl(INADDR_ANY);
       addr.sin_port = htons(gChat);
       addrlen = sizeof(addr);
       u_int yes = 1;

        if( setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) <0 ) {
            write(ttyfd, "set SO_REUSEADDR option failed\n ", strlen("set SO_REUSEADDR option failed\n "));
        }
        if( setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) <0 ) {
            write(ttyfd, "set SO_BROADCAST option failed\n ", strlen("set SO_BROADCAST option failed\n "));
        }

        if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
                write(ttyfd, "bind error\n", strlen("bind error\n"));
                return 0;
        } else {
           write(ttyfd, "group socket binded\n", strlen("group socket binded\n"));
       }

       return sock;
}

void socketRxMessage(char buffer[]) {
//    printf("socketRxMessage: %s\n", buffer);
    if(buffer[0] == 'u') {
               pvChat =  (uint8_t)buffer[1];
               write(ttyfd, "Joined successfully\n", strlen("Joined successfully\n"));
    } else if(buffer[0] == 'g') {
        gChat =  (uint8_t)buffer[1]*256 + (uint8_t)buffer[2];
        write(ttyfd, "Joined successfully\n", strlen("Joined successfully\n"));
        gSocketfd = createGroupSocket() ;
    } else if(buffer[0] == 'C') {
        pvChat = 0;
        write(ttyfd, "Private chat Exit\n", strlen("Private chat Exit\n"));
    } else if(buffer[0] == GROUP_UN_ACTIVE && gChat > 0) {
        close(gSocketfd);
        gChat = 0;
        gSocketfd = 0;
//        printf("Group unactive from server\n");
    } else {
        write(ttyfd, buffer, strlen(buffer));
        write(ttyfd, " \n", strlen(" \n"));
    }
}

void commandHandler(char buff[], int sockfd) {
    char message[100];
    if(pvChat > 0) {
        if(buff[0] == PRIVATE_CHAT_EXIT) {
            pvChat = 0;
            printf("PVChat exited\n");
        } else {
            message[0] = PRIVATE_CHAT; message[1] = (char)pvChat;
            strcat(message, buff);
            if(sendToServer(sockfd, message) < 0 ) {
                write(ttyfd, "error in sending data\n", strlen("error in sending data\n"));
            }
            bzero(message, strlen(message));
        }

    } else if(gChat > 0) {
        if(buff[0] == GROUP_EXIT) {
           message[0] = GROUP_EXIT;
           message[1] = gChat/256;
           message[2] = gChat%256;
           sendToServer(sockfd, message);
           close(gSocketfd);
           gChat = 0;
           gSocketfd = 0;
        } else {
            write(ttyfd, "send group message\n", strlen("send group message\n"));
            sendGroupMessage(gSocketfd, buff) ;
        }

    } else {
        if(sendToServer(sockfd, buff) < 0 ) {
            write(ttyfd, "error in sending data\n", strlen("error in sending data\n"));
        }
    }
}

void refreshGroupActivity() {
//    printf("refreshGroupActivity\n");
    if(gChat>0) {
        char message[3];
        message[0] = GROUP_REFRESH;
        message[1] = gChat/256;
        message[2] = gChat%256;
        if(sendToServer(serverSocketfd, message) < 0 ) {
//            printf("error in refreshGroupActivity\n");
        } else {
//            printf("ref/*r*/eshGroupActivity\n");
        }
    }
    alarm(25);
    signal(SIGALRM, refreshGroupActivity);
//    printf("refreshGroupActivity Exit\n");
}


int main(int argc, char *argv[])
{
    ttyfd = open("/dev/tty", O_RDWR);
    char buffer[MAX_BUFFER_SIZE] = {0};
    int sockfd;
    struct sockaddr_in serv_addr;
    int activity;
    fd_set readfds;
    int max_fd ;

        if (argc<2)
        {
            fprintf (stderr,"SORRY! Provide A Port ! \n");
            return 1;
        }


    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    serverSocketfd = sockfd;
    if (sockfd<0) {
        fprintf (stderr,"SORRY! Cannot create a socket ! \n");
        return 1;
    }

    memset(&serv_addr,0,sizeof serv_addr);

    int  portno = atoi(argv[1]);
//    int portno = PORT;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr *)&serv_addr,   sizeof(serv_addr))) {
        write(ttyfd, "Connection Failed \n", strlen("Connection Failed \n"));
        return -1;
    } else {
        write(ttyfd, "connected to server\n", strlen("connected to server\n"));
    }
    int rcv_id ;

    char c;
    int n = 0;
    char buff[MAX_BUFFER_SIZE];
//    alarm(25);
//    signal(SIGALRM, refreshGroupActivity);
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);
        FD_SET(sockfd, &readfds);
        max_fd = sockfd;
        if(STDIN_FILENO > max_fd) {
            max_fd = STDIN_FILENO;
        }
        if(gSocketfd > 0) {
           FD_SET(gSocketfd, &readfds);
           if(max_fd < gSocketfd) {max_fd = gSocketfd;}
        }

        activity = select( max_fd + 1 , &readfds , NULL , NULL , NULL);

        if ((activity < 0) && (errno!=EINTR))
               {
                   write(ttyfd, "select error", strlen("select error"));
               }


               if (FD_ISSET(sockfd, &readfds))
               {
                   if ((rcv_id =recv( sockfd , buffer, 1024, 0)) >=0)
                   {
                       socketRxMessage(buffer);
                       bzero(buffer, sizeof(buffer));
                   }
               }

               if (FD_ISSET(STDIN_FILENO, &readfds)) {
                     if (read(STDIN_FILENO, &c, sizeof(c)) > 0) {
                         if(c != '\n') {
                            buff[n++] = c;
                         } else {
                            n = 0;
                            commandHandler(buff, sockfd);
                            bzero(buff, sizeof(buff));
                         }
                      }
                }
               if(gSocketfd > 0) {
                   if (FD_ISSET(gSocketfd, &readfds))
                   {
                       if ((rcv_id =recv( gSocketfd , buffer, 1024, 0)) >=0)
                       {
                           write(ttyfd, "group message:", strlen("group message:"));
                           write(ttyfd, buffer, strlen(buffer));
                           write(ttyfd, "\n", strlen("\n"));
                       }
                   }
               }

    }

    close(sockfd);
    return 0;
}

//-------------------------------------
//GET_GROUPS_LIST: G, JOIN_GROUP: J$(group name), CREATE_GROUP: N$(group name),
// JOIN_PRIVATE_CHAT: P$(user name), GROUP_EXIT: E, GET_USERS_LIST: U,
// PRIVATE_CHAT_EXIT: C,  PRIVATE_CHAT 'S' ,GROUP_CHAT 'T'

//G
// return  lggroup1,group2,  // lg: packet code

//Jgroup1 // join group1
// return g1joined succefully   // g: group packet code // 1: group id

// Ngroup3 // create new group3
// return  Group created succefully

// PMohammad  // join to private chat
// return u1 // u: user packet code // 1: user id

// E  // exit from group, we need it for usercounter
// return Exited

// U
// return luuser1,user2  // lu: user list

// C
// return exit from PV // just in client

// S5$(Message) // S: packetCode, 5: user id, $(Message):
// if(not successfull) { return user disconnected;}

// T4$(Message) // T: packetDode, 4: group id, $(Message)


// ****************** use free
