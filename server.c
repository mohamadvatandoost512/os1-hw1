#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <netdb.h>
#include <fcntl.h>
#include <signal.h>

#define PORT 6574
#define MAX_BUFFER_SIZE 5000

#define GET_GROUPS_LIST 'G'
#define GET_USERS_LIST 'U'
#define JOIN_GROUP 'J'
#define CREATE_GROUP 'N'
#define JOIN_PRIVATE_CHAT 'P'
#define GROUP_EXIT 'E' 
#define PRIVATE_CHAT_EXIT 'C'

#define PRIVATE_CHAT 'S'
#define GROUP_CHAT 'T'

#define GROUP_UN_ACTIVE 'F'
#define GROUP_REFRESH 'R'

// use mmap for shared memory

int ttyfd, logfd;

struct User {
    char username[20];
    int socketfd;
    uint8_t userId;
};

struct Group {
   char groupName[20];
   struct User users[20];
   int userOnline;
   int groupId;
   int portNum;
   int sockId;
   int active;
};

struct User users[100];

struct Group groups[10];

int groupCounter = 0;
int userNumbers = 0;
int usersId = 1;
int groupsid = 1;

int isAuthenticated[20];
int groupPortNum = 4562;

// set socket add option reuse and broadcast and address IP

int sendToClient(int clientfd, char buf_send[])
{
        if ( send(clientfd, (void *) (buf_send), strlen(buf_send), 0) < 0) {
            return -1;
        }
     return 0;
}

int sendToClientFixdSize(int clientfd, char buf_send[], int buf_size)
{
        if ( send(clientfd, (void *) (buf_send), buf_size, 0) < 0) {
            return -1;
        }
     return 0;
}

void createGroup(char groupName[], int clientfd) {
    write(ttyfd, "createGroup :\n", strlen("createGroup :\n"));
    struct sockaddr_in addr;
    int addrlen, sock, cnt;
    struct ip_mreq mreq;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
       if (sock < 0) {
         write(ttyfd, "socket :\n", strlen("socket :\n"));
         exit(1);
       }
       bzero((char *)&addr, sizeof(addr));
       addr.sin_family = AF_INET;
       addr.sin_addr.s_addr = htonl(INADDR_ANY);
       addr.sin_port = htons(groupPortNum);
       addrlen = sizeof(addr);
       u_int yes = 1;
       if( setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) <0 ) {
           write(ttyfd, "set SO_REUSEADDR option failed\n", strlen("set SO_REUSEADDR option failed\n"));
       }
       if( setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) <0 ) {
           write(ttyfd, "set SO_BROADCAST option failed\n", strlen("set SO_BROADCAST option failed\n"));
       }

    strcpy(groups[groupCounter].groupName, groupName);
    groups[groupCounter].userOnline = 0;
    groups[groupCounter].groupId = groupsid;
    groups[groupCounter].sockId = sock;
    groups[groupCounter].active = 1;
    groups[groupCounter].portNum = groupPortNum ; groupPortNum++;
    groupsid++;
    groupCounter++;
    sendToClient(clientfd,"Group created succefully\n");
}

void joinGroup(char groupName[], struct User user, int clientfd) {
    write(ttyfd, "joinGroup :\n", strlen("joinGroup :\n"));
    char message[3];
    for(int i=0; i<groupCounter; i++) {
        if (strcmp(groupName, groups[i].groupName) == 0) {
              groups[i].users[groups[i].userOnline] = user ;
              groups[i].userOnline++;
              message[0] = 'g';
              message[1] = groups[i].portNum/256;
              message[2] = groups[i].portNum%256;
              sendToClient(clientfd,message);
              sendToClientFixdSize(clientfd,message,3);
              // free message
              return;
        }
    }
    // free message
    sendToClient(clientfd,"your group does not exist\n");
}

char* getGroupsList() {
    write(ttyfd, "getGroupsList :\n", strlen("getGroupsList :\n"));
    char *groupsList = malloc(100);
//    strcat(groupsList,"lg");
    for(int i=0; i<groupCounter; i++) {
        strcat(groupsList,groups[i].groupName);
        strcat(groupsList,",");
    }

    return groupsList;
}

char* getUsersList()
{
    write(ttyfd, "getUsersList :\n", strlen("getUsersList :\n"));
    char *usersList = malloc(100);
    for(int i=0; i<userNumbers; i++) {
        strcat(usersList, users[i].username);
        strcat(usersList,",");
    }
    return usersList;
}

void send_PvMessage(int senderfd, int recieverId, char buf_send[]) {
    write(ttyfd, "send_PvMessage :\n", strlen("send_PvMessage :\n"));
    char message[100];
    bzero(message, strlen(message));
    for(int i=0; i<userNumbers; i++) {
        if(users[i].socketfd == senderfd) {
            strcat(message, users[i].username);
            strcat(message, ":");
            strcat(message, buf_send);
            for(int j=0; j<userNumbers;j++) {
                if(users[j].userId == recieverId) {
                    if(sendToClient(users[j].socketfd, message) < 0) {
                        write(ttyfd, "pvmessage does not send\n", strlen("pvmessage does not send\n"));
                    }
                    break;
                }
            }
            break;
        }
    }
}

void joinPrivateChat(char userName[], int clientfd) {
    write(ttyfd, "joinPrivateChat :", strlen("joinPrivateChat :"));
    write(ttyfd, userName, strlen(userName));
    char message[2];
    message[0] = 'u';
    for(int i=0; i<userNumbers; i++) {
//        printf("users: %s\n", users[i].username);
        if(strcmp(users[i].username, userName) == 0) {
//            printf("user founded\n");
//            strcat(message, (char)users[i].userId);
            message[1] = users[i].userId;
            sendToClientFixdSize(clientfd, message,2);
            // free
            return;
        }
    }

    printf("joinPrivateChat user not found\n");
}

void sendGroupMessage(int senderfd, int groupId, char buf_send[])
{
    char message[100];
    for(int i=0; i<userNumbers; i++) {
        if( users[i].socketfd == senderfd ) {
            strcat(message, users[i].username);
            strcat(message, ":");
        }
    }
    strcat(message, buf_send);
    for(int i=0; i<groupCounter; i++) {
        if (groups[i].groupId == groupId) {
              for(int j=0; j<groups[i].userOnline; j++) {
                 if(groups[i].users[j].socketfd != senderfd) {
                    sendToClient(groups[i].users[j].socketfd, message);
                 }            
              }
              // free message
              return;
        }
    }
    sendToClient(senderfd, "Group is not active\n");
}

int send_help(int clientfd)
{
   char buf_send[] = "GET_GROUPS_LIST: G, JOIN_GROUP: J$(group name), CREATE_GROUP: N$(group name), JOIN_PRIVATE_CHAT: P$(user name), GROUP_EXIT: E, GET_USERS_LIST: U, PRIVATE_CHAT_EXIT: C \n";
   if ( send(clientfd, (void *) (buf_send), strlen(buf_send), 0) < 0) {
       return -1;
   }
   return 0;
}

void groupExit(int clientfd, int portNum) {
    write(ttyfd, "groupExit \n", strlen("groupExit \n"));
    int t=0;
   for(int i=0; i<groupCounter; i++) {
    if(groups[i].portNum == portNum) {
        for(int j=0; j<groups[i].userOnline; j++) {
            if(groups[i].users[j].socketfd != clientfd) {
               groups[i].users[t] =  groups[i].users[j];
               t++;
            } else {
                groups[i].userOnline = groups[i].userOnline - 1 ;
            }
        }
        if(groups[i].userOnline == 0) {
            close(groups[i].sockId);
            int k =0;
            for(int j=0;j<groupCounter; j++) {
                if(groups[i].userOnline == 0) {

                } else {
                    groups[k] = groups[j];
                    k++;
                }
            }
        }
        return;
    }
   }
}

void clientSocketHandler(int clientfd, int index, char buf_rcv[])
{

            if(isAuthenticated[index] == 0) {
                strcpy(users[userNumbers].username, buf_rcv);
                users[userNumbers].socketfd = clientfd;
                users[userNumbers].userId = usersId;
                usersId++;
                userNumbers++;
                isAuthenticated[index] = 1;
                send_help(clientfd);
            } else {
                char buf_message[100];
                switch ((char)buf_rcv[0])
                {
                    case GET_GROUPS_LIST:
                        write(logfd, "GET_GROUPS_LIST request\n", strlen("GET_GROUPS_LIST request\n"));
                        sendToClient(clientfd, getGroupsList());
                        break;
                    case JOIN_GROUP:
                        write(logfd, "JOIN_GROUP request\n", strlen("JOIN_GROUP request\n"));
                        for(int t=1; t<strlen(buf_rcv); t++) {
                          buf_message[t-1] = buf_rcv[t];
                        }
                        for(int i=0; i<userNumbers;i++) {
                          if(users[i].socketfd == clientfd) {
                              joinGroup(buf_message, users[i], clientfd);
                          }
                        }
                        break;
                    case CREATE_GROUP:
                        write(logfd, "CREATE_GROUP request\n", strlen("CREATE_GROUP request\n"));
                        for(int t=1; t<strlen(buf_rcv); t++) {
                           buf_message[t-1] = buf_rcv[t];
                        }
                        createGroup(buf_message, clientfd);
                        break;
                    case JOIN_PRIVATE_CHAT:
                        write(logfd, "JOIN_PRIVATE_CHAT request\n", strlen("JOIN_PRIVATE_CHAT request\n"));
                        for(int t=1; t<strlen(buf_rcv); t++) {
                            buf_message[t-1] = buf_rcv[t];
                        }
                        joinPrivateChat(buf_message, clientfd);
                        break;
                    case PRIVATE_CHAT:
                        write(logfd, "PRIVATE_CHAT request\n", strlen("PRIVATE_CHAT request\n"));
                        //S5$(Message)
                        for(int t=2; t<strlen(buf_rcv); t++) {
                            buf_message[t-2] = buf_rcv[t];
                        }
                        send_PvMessage(clientfd, buf_rcv[1], buf_message);
                        break; 
                    case GROUP_CHAT:
                        write(logfd, "GROUP_CHAT request\n", strlen("GROUP_CHAT request\n"));
                        //T4$(Message)
                        for(int t=2; t<strlen(buf_rcv); t++) {
                           buf_message[t-2] = buf_rcv[t];
                        }
                        sendGroupMessage(clientfd, (uint8_t)buf_rcv[1], buf_message);
                        break;       
                    case GROUP_EXIT:
                        write(logfd, "GROUP_EXIT request\n", strlen("GROUP_EXIT request\n"));
                        groupExit(clientfd, (uint8_t)buf_rcv[1]*256 + (uint8_t)buf_rcv[2]);
                        break;
                    case GET_USERS_LIST:
                        write(logfd, "GET_USERS_LIST request\n", strlen("GET_USERS_LIST request\n"));
                        sendToClient(clientfd, getUsersList());
                        break;
                    default:
                        write(logfd, "Not vali request\n", strlen("Not vali request\n"));
                        sendToClient(clientfd, " Not valid code ! \n");
                }

                bzero(buf_message, strlen(buf_message));

            }

}

void checkGroupActivity()
{
    printf("checkGroupActivity\n");
    for(int i=0; i<groupCounter; i++) {
        if(groups[i].active == 0) {
            printf("group unactived\n");
            for(int j=0;j<groups[i].userOnline; j++) {
                sendToClient(groups[i].users[j].socketfd, "FGroup unactive");
            }
            close(groups[i].sockId);
            int k =0;
            for(int j=0;j<groupCounter; j++) {
                if(groups[i].userOnline == 0) {

                } else {
                    groups[k] = groups[j];
                    k++;
                }
            }
            groupCounter--;
        } else {
            groups[i].active = 0;
        }
    }
    alarm(30);
    signal(SIGALRM, checkGroupActivity);
}

int main(int argc, char *argv[])
{
        ttyfd = open("/dev/tty", O_RDWR);
        logfd = open("serverLog.txt", O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
        int sockfd,newsockfd;
        struct sockaddr_in serv_addr;
        struct sockaddr cli_addr;
        int activity;
        fd_set readfds;
        int max_fd ;
        int maxOfClients = 20;
        int clients_fd[maxOfClients];
        char rx_buffer[MAX_BUFFER_SIZE];

        for(int i=0; i<maxOfClients; i++) {
            clients_fd[i] = 0; isAuthenticated[i] = 0;
        }

        if (argc<2)
        {
            write(ttyfd, "SORRY! Provide A Port ! \n", 30);
            return 1;
        }


        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sockfd<0) {
            write(ttyfd, "SORRY! Cannot create a socket ! \n", strlen("SORRY! Cannot create a socket ! \n"));
            return 1;
        }

        memset(&serv_addr,0,sizeof serv_addr);

        int  portno = atoi(argv[1]);
//        int portno = PORT;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(portno);


        int binded = bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

        if (binded <0 ) {
            fprintf (stderr,"Error on binding! \n");
            return 1;
        }

        listen(sockfd, maxOfClients);

        int clilen = sizeof(struct sockaddr);
        write(ttyfd, "waiting for connection:\n", strlen("waiting for connection:\n"));
        write(logfd, "Socket created successfylly\n", strlen("Socket created successfylly\n"));

//        alarm(30);
//        signal(SIGALRM, checkGroupActivity);

        while(1) {

            FD_ZERO(&readfds);
            FD_SET(SIGALRM, &readfds);
            FD_SET(sockfd, &readfds);
            max_fd = sockfd;

            for (int i = 0 ; i < maxOfClients ; i++)
            {
               if(clients_fd[i] > 0)  {
                   FD_SET( clients_fd[i] , &readfds);
               }

               if(clients_fd[i] > max_fd)  {
                   max_fd = clients_fd[i];
               }

            }

             activity = select( max_fd + 1 , &readfds , NULL , NULL , NULL);

                    if ((activity < 0) && (errno!=EINTR))
                    {
                         write(ttyfd, "select error\n", strlen("select error\n"));
                    }


                    if (FD_ISSET(sockfd, &readfds))
                    {
                        if ((newsockfd = accept(sockfd, &cli_addr, (socklen_t*) &clilen))<0)
                        {
                            write(ttyfd, "accept error\n", strlen("accept error\n"));
                            exit(EXIT_FAILURE);
                        }
                        write(logfd, "new socket accept\n", strlen("new socket accept\n"));
                        if(sendToClient(newsockfd, "Enter your username:\n")<0) {
                            write(ttyfd, "Error in sending to client ! \n", strlen("Error in sending to client ! \n"));
                            exit (1);
                        }

                        for (int i = 0; i < maxOfClients; i++)
                        {
                            if( clients_fd[i] == 0 )
                            {
                                clients_fd[i] = newsockfd;
                                break;
                            }
                        }
                    }


                    for (int i = 0; i < maxOfClients; i++)
                    {
                        int sd = clients_fd[i];

                        if (FD_ISSET( sd , &readfds))
                        {
                             int value = 0;
                             bzero(rx_buffer, MAX_BUFFER_SIZE);
                            if ((value = read( sd , rx_buffer, MAX_BUFFER_SIZE)) == 0)
                            {
                                close( sd );
                                clients_fd[i] = 0;
                                isAuthenticated[i] = 0;
                            } else {
                                // valid data
                                clientSocketHandler(clients_fd[i], i, rx_buffer);
//                                rx_buffer[value] = '\0';
//                                send(sd , rx_buffer , strlen(rx_buffer) , 0 );
                            }
                        }
                    }

//                    if(FD_ISSET(SIGALRM, &readfds)) {
//                        checkGroupActivity();
//                    }
        }

        close(sockfd);

    return 0;
}


//-------------------------------------
//GET_GROUPS_LIST: G, JOIN_GROUP: J$(group name), CREATE_GROUP: N$(group name), 
// JOIN_PRIVATE_CHAT: P$(user name), GROUP_EXIT: E, GET_USERS_LIST: U, 
// PRIVATE_CHAT_EXIT: C,  PRIVATE_CHAT 'S' ,GROUP_CHAT 'T'


//G 
// return  lggroup1,group2,  // lg: packet code

//Jgroup1 // join group1
// return g1joined succefully   // g: group packet code // 1: group id 

// Ngroup3 // create new group3 
// return  Group created succefully

// PMohammad  // join to private chat
// return u1 // u: user packet code // 1: user id

// E  // exit from group, we need it for usercounter
// return Exited

// U 
// return luuser1,user2  // lu: user list

// C 
// return exit from PV // just in client

// S5$(Message) // S: packetCode, 5: user id, $(Message): 
// if(not successfull) { return user disconnected;}

// T4$(Message) // T: packetDode, 4: group id, $(Message)




// ****************** use free
